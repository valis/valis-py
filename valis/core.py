import glob
import os
from pathlib import Path
from typing import List, Union

from command_runner import command_runner

Pathlike = Union[str, Path]


def run(command: str, echo=True) -> str:
    """Run a command in shell mode with live output."""
    result = command_runner(command, shell=True, live_output=echo)
    return str(result[1]).strip()


def exists(command: str) -> bool:
    result = command_runner(command, shell=True, live_output=False)
    return result[0] == 0


def save(filename: Pathlike, content: str):
    with open(filename, "w") as text_file:
        text_file.write(content)


def read(filename: Pathlike) -> str:
    with open(filename, "r") as file:
        content = file.read()
    return content


def find_files(pattern: str) -> List[str]:
    """Returns list of paths for all files matching a pattern"""
    return glob.glob(pattern)


def number_of_files(path: Pathlike) -> int:
    """Return the number of files in a certain path (glob supported)"""
    return len(glob.glob(os.path.expanduser(path)))


def get_kpx_attribute(db, key_file, entry, attribute):
    return run(
        f"keepassxc-cli show --no-password -k {key_file} -sa {attribute} {db} {entry}"
    ).strip()


OS = run("uname -s", echo=False).strip()
FEDORA = Path("/etc/fedora-release").exists()
UBUNTU = Path("/etc/lsb-release").exists()

import os
from typing import Optional
from dataclasses import dataclass

from rich import print

from . import core
from . import log


@dataclass
class WasabiConnectionInfo:
    bucket: Optional[str]
    access_key: str
    secret_key: str
    endpoint: str
    password: str

def read_wasabi_variables() -> WasabiConnectionInfo:
    return WasabiConnectionInfo(
            bucket = os.getenv("WASABI_KOPIA_BUCKET"),
            access_key=os.getenv("WASABI_KOPIA_ACCESS_KEY", ""),
            secret_key=os.getenv("WASABI_KOPIA_SECRET_KEY", ""),
            endpoint=os.getenv("WASABI_KOPIA_ENDPOINT", ""),
            password=os.getenv("KOPIA_PASSWORD", ""))


def connect_s3(info: WasabiConnectionInfo) -> None:
    """Connect to a repository in an S3 bucket"""
    log.info(
        f"[green]Kopia:[/green] connecting to repository [yellow]{info.endpoint}:{info.bucket}[/yellow]"
    )
    core.run(
        f"kopia repository connect s3 --bucket='{info.bucket}' --access-key='{info.access_key}' --secret-access-key='{info.secret_key}' --endpoint='{info.endpoint}' --password='{info.password}'"
    )


def connect_b2(bucket, access_key, secret_key, password):
    """Connect to a repository in a B2 bucket"""
    log.info(
        f"[green]Kopia:[/green] connecting to repository [yellow]B2:{bucket}[/yellow]"
    )
    core.run(
        f"kopia repository connect b2 --bucket='{bucket}' --key-id='{access_key}' --key='{secret_key}' --password='{password}'"
    )


def connect_local(path, password):
    """Connect to a local repository"""
    print(f"[green][kopia][/green] connecting to [yellow]local volume[/yellow]")
    core.run(
        f"kopia repository connect filesystem --path={path} --password='{password}'"
    )


def create_snapshot(area: str):
    """Create a snapshot to """
    CURRENT_REPO = core.run("kopia repository status | awk 'NR==3' | cut -c 36-")
    print(
        f"[green][kopia][/green] Backing up {area} to [yellow]{CURRENT_REPO}[/yellow]"
    )
    core.run(f"kopia snapshot create {area}")


def kopia_b2_connect():
    connect_b2(
        bucket=os.getenv("B2_KOPIA_BUCKET"),
        access_key=os.getenv("B2_KOPIA_ACCESS_KEY"),
        secret_key=os.getenv("B2_KOPIA_SECRET_KEY"),
        password=os.getenv("KOPIA_PASSWORD"),
    )


def kopia_local_connect(path):
    if not path:
        path = "/Volumes/Backup/backup"
    connect_local(
        path=path,
        password=os.getenv("KOPIA_PASSWORD"),
    )


def kopia_backup():
    """Backup the selected areas to the active repository"""
    areas = [
        "/Sync/code",
        "/Sync/documents",
        "/Sync/music",
        "/Sync/sites",
        "/Documents/Rack2",
        "/Maildir",
        "/notes",
    ]
    HOME = os.getenv("HOME")
    if HOME:
        for area in areas:
            create_snapshot(HOME + area)

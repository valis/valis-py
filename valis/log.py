from rich import print


def info(message: str, symbol=None) -> None:
    if not symbol:
        symbol = "📢"
    print(f"[{symbol}] [yellow]{message}[/yellow]")


def ok(message: str):
    print(f"[👍] [green]{message}[/green]")


def error(message: str):
    print(f"[💀] [red]{message}[/red]")


def warning(message: str):
    """Log a warning message"""
    print(f"[🎃] [yellow]{message}[/]")


def header(message: str):
    print(f"= [blue]{message}[/blue] " + "=" * (70 - len(message)))


def shell(message: str):
    info(message=message, symbol="🐚")
